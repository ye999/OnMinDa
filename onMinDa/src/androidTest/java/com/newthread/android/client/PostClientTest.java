package com.newthread.android.client;

import android.os.SystemClock;
import android.test.ApplicationTestCase;
import android.test.suitebuilder.annotation.LargeTest;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.datatype.BmobRelation;
import cn.bmob.v3.listener.DeleteListener;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;
import com.newthread.android.activity.main.MyApplication;
import com.newthread.android.bean.remoteBean.StudentUser;
import com.newthread.android.bean.remoteBean.bbs.Post;
import com.newthread.android.manager.BmobRemoteDateManger;
import com.newthread.android.util.Loger;

import java.util.List;

public class PostClientTest extends ApplicationTestCase<MyApplication> {


    public PostClientTest() {
        super(MyApplication.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        createApplication();
    }

    /**
     * 增加帖子
     *
     * @throws Exception
     */
    @LargeTest
    public void testAdd(int i) throws Exception {
        final Post post = new Post();
        post.setTitle("可以啊小伙子"+i);
        post.setContent("测试");
        BmobQuery<StudentUser> querry = new BmobQuery<>();
        querry.addWhereEqualTo("username", "2012213738");
        querry.addWhereEqualTo("password", "233233");
        querry.findObjects(getApplication(), new FindListener<StudentUser>() {
            @Override
            public void onSuccess(List<StudentUser> list) {
                post.setAuthor(list.get(0));
                BmobRemoteDateManger.getInstance(getApplication()).add(post, new SaveListener() {
                    @Override
                    public void onSuccess() {
                        Loger.V("保存post成功");
                    }

                    @Override
                    public void onFailure(int i, String s) {
                        Loger.V("保存post失败" + i + " " + s);
                    }
                });
            }

            @Override
            public void onError(int i, String s) {
                Loger.V("查询用户失败" + i + " " + s);
            }
        });
        SystemClock.sleep(4000);
    }

    /**
     * 一次性增加50条数据
     * testAdd()里面的 SystemClock.sleep(。。);要注释
     *
     * @throws Exception
     */
    @LargeTest
    public void testAdd50() throws Exception {
        for (int i = 0; i < 50; i++) {
            testAdd(i);
        }
        SystemClock.sleep(10000);
    }

    /**
     * 更新帖子收藏
     *
     * @throws Exception
     */
    @LargeTest
    public void testUpdate() throws Exception {
        Post post = new Post();
        post.setObjectId("9cfd35a6d3");
        BmobRelation bmobRelation = new BmobRelation();
        StudentUser studentUser1 = new StudentUser(null, null);
        StudentUser studentUser2 = new StudentUser(null, null);
        studentUser1.setObjectId("0115323658");
        studentUser2.setObjectId("60cdcc59f8");
        bmobRelation.add(studentUser1);
        bmobRelation.add(studentUser2);
        post.setLikes(bmobRelation);
        BmobRemoteDateManger.getInstance(getApplication()).update(post, new UpdateListener() {
            @Override
            public void onSuccess() {
                Loger.V("更新成功");
            }

            @Override
            public void onFailure(int i, String s) {
                Loger.V("更新失败" + i + " " + s);
            }
        });
        SystemClock.sleep(4000);
    }

    /**
     * 分页查询
     *
     * @throws Exception
     */
    @LargeTest
    public void testQuerry() throws Exception {
        BmobQuery<Post> query = new BmobQuery<>();
        query.order("-createdAt");
        query.include("author,type");
        query.setLimit(20);
        query.findObjects(getApplication(), new FindListener<Post>() {
            @Override
            public void onSuccess(List<Post> list) {
                for (Post post : list) {
                    Loger.V(post);
                }
            }

            @Override
            public void onError(int i, String s) {
                Loger.V("查询失败" + i + s);
            }
        });
        SystemClock.sleep(4000);
    }

    /**
     * 删除帖子
     *
     * @throws Exception
     */
    @LargeTest
    public void testDelete() throws Exception {
        Post post = new Post();
        post.setObjectId("b6b61b3dfa");
        BmobRemoteDateManger.getInstance(getApplication()).delete(post, new DeleteListener() {
            @Override
            public void onSuccess() {
                Loger.V("删除成功");
            }

            @Override
            public void onFailure(int i, String s) {
                Loger.V("删除失败" + i + s);
            }
        });
        SystemClock.sleep(4000);
    }

    @Override
    public void tearDown() throws Exception {

    }
}