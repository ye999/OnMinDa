package com.newthread.android.ui.bbs;

import android.os.Bundle;
import com.newthread.android.activity.main.BaseSFActivity;
import com.newthread.android.activity.main.MyApplication;
import com.newthread.android.bean.StudentUserLocal;
import com.newthread.android.util.Loger;

/**
 * Created by jindongping on 15/6/24.
 */
public class BbsUserInfoActivity extends BaseSFActivity {

    private StudentUserLocal user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user= MyApplication.getInstance().getThing("toUser");
        Loger.V(user);
    }
}
