package com.newthread.android.dialog;

import com.newthread.android.dialog.dialoginformation.DiaLogType;

import java.util.HashMap;
import java.util.Map;


public class DiaLogListenerManager {
	private static class DiaLogListenerManagerHolder {
		static DiaLogListenerManager instance = new DiaLogListenerManager();
	}

	public static DiaLogListenerManager getInstance() {
		return DiaLogListenerManagerHolder.instance;
	}

	private Map<Class<?>, OnDiaLogListener> diaLogListeners = new HashMap<Class<?>, OnDiaLogListener>();

	public Map<Class<?>, OnDiaLogListener> getDiaLogListeners() {
		return diaLogListeners;
	}

	public void addDiaLogListener(OnDiaLogListener diaLogListener,
			DiaLogConfig diaLogConfig) {
		Class<?> dialogType = diaLogConfig.getDialogClassType();
		if (isDialogShowing(dialogType)) {
			return;
		}
		diaLogListeners.put(dialogType, diaLogListener);
	}

	public <T extends OnDiaLogListener> T removeDiaLogListener(DiaLogType dialogType) {

		return (T) diaLogListeners.remove(dialogType.getDialogClassType());

	}

	private boolean isDialogShowing(Class<?> dialogType) {
		return diaLogListeners.containsKey(dialogType);
	}

}
