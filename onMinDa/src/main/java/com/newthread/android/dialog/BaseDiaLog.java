package com.newthread.android.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.Window;
import android.view.WindowManager;

public abstract class BaseDiaLog<T extends DiaLogConfig> extends Activity {

	private T diaLogConfig;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		WakeLock mWakelock = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP
				| PowerManager.SCREEN_DIM_WAKE_LOCK, "SimpleTimer");
		mWakelock.acquire();
		mWakelock.release();
		final Window win = getWindow();
		win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
				| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
		win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
				| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

		diaLogConfig = (T) getIntent().getSerializableExtra(
				DiaLogConfig.INTENT_EXTRA_CONFIG_NAME);
		initContentView();
		onInit(savedInstanceState);

	}

	protected void setDialogResult(int resultCode) {
		setDialogResult(resultCode, null/* intent */);
	}

	protected void setDialogResult(int resultCode, Intent intent) {
		if (intent == null) {
			intent = new Intent();
		}
		setResult(resultCode, intent);
		finish();
	}
	
	protected <T extends OnDiaLogListener > T getDiaLogListener(){
		return DiaLogListenerManager.getInstance().removeDiaLogListener(diaLogConfig.getDiaLogType());
	}
	
	public abstract void initContentView();
	public abstract void onInit(Bundle savedInstanceState);
	public T getDiaLogConfig() {
		return diaLogConfig;
	}

}
