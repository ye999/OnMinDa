package com.newthread.android.manager.iMangerService;

import cn.bmob.v3.listener.DeleteListener;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;
import com.newthread.android.bean.remoteBean.RemoteObject;


/**
 * Created by jindongping on 14/12/9.
 * 采用Bomb数据的回调函数  自己懒得写了
 */
public interface IRemoteService<T extends RemoteObject> {
    void add(T t, SaveListener saveListener);

    void update(T t, UpdateListener updateListener);

    void querry(T t, FindListener<T> findListener);

    void delete(T t, DeleteListener deleteListener);
}
