package com.newthread.android.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.newthread.android.R;
import com.newthread.android.bean.StudentUserLocal;
import com.newthread.android.util.ImageUtil;
import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.annotation.view.ViewInject;

import java.util.List;

/**
 * Created by jindongping on 15/6/4.
 */
public class BbsGroupEpListAdapter extends BaseExpandableListAdapter {
    //重写ExpandableListAdapter中的各个方法
    private String[] generalsTypes = new String[]{""};
    //子视图显示文字
    private List<List<StudentUserLocal>> generals;
    private LayoutInflater inflater;

    public BbsGroupEpListAdapter(String[] generalsTypes, List<List<StudentUserLocal>> generals, LayoutInflater inflater) {
        this.generalsTypes = generalsTypes;
        this.generals = generals;
        this.inflater = inflater;
    }

    @Override
    public int getGroupCount() {
        return generalsTypes.length;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return generalsTypes[groupPosition];
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return generals.get(groupPosition).size();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return generals.get(groupPosition).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.bbs_group_father_item, parent, false);
            holder = new GroupViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (GroupViewHolder) convertView.getTag();
        }
        //设置holder数据
        holder.title.setText(generalsTypes[groupPosition]);
        return convertView;
    }

    static class GroupViewHolder {
        @ViewInject(id = R.id.bbs_group_father_title)
        private TextView title;

        public GroupViewHolder(View view) {
            FinalActivity.initInjectedView(this,view);
        }
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.bbs_group_son_item, parent,false);
            holder = new ChildViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ChildViewHolder) convertView.getTag();
        }
        StudentUserLocal studentUserLocal = generals.get(groupPosition).get(childPosition);
        ImageUtil.getInstance(inflater.getContext()).disPalyImage(studentUserLocal.getHeadPhotoUrl(), holder.headPhoto);
        holder.name.setText(studentUserLocal.getName());
        //设置holder数据
        return convertView;
    }

    static class ChildViewHolder {
        @ViewInject(id=R.id.bbs_group_son_headPhoto)
        private ImageView headPhoto;
        @ViewInject(id=R.id.bbs_group_son_name)
        private TextView name;

        public ChildViewHolder(View view) {
            FinalActivity.initInjectedView(this,view);
        }
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}